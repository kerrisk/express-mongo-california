const apiResponse = require("../helpers/apiResponse");
const authenticatePrivileges = function (req, res, next) {
  if (req.user.isAdmin) next();
  else apiResponse.unauthorizedResponse(res, "El usuario no es administrador");
};

module.exports = authenticatePrivileges;
