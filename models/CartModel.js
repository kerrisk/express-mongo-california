var mongoose = require("mongoose");
const { ObjectID, Da } = require("mongoose/lib/schema/index");

var CartSchema = new mongoose.Schema(
  {
    idUsuario: { type: ObjectID, required: true },
    productos: { type: Array, required: true },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Cart", CartSchema);
