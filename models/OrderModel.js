var mongoose = require("mongoose");
const { ObjectID, Date, Decimal } = require("mongoose/lib/schema/index");
const Decimal128 = require("mongoose/lib/schema/decimal128");

var OrderSchema = new mongoose.Schema(
  {
    idUsuario: { type: ObjectID, required: true },
    Usuario: { type: Object, required: true },
    idEmpleado: { type: ObjectID, required: false },
    Empleado: { type: Object, required: false },
    productos: { type: Array, required: true },
    fecha: { type: Date, required: true },
    total: { type: Number, required: true },
    horaEstimada: { type: String, required: false },
    fechaEntrega: { type: Date, required: false },
    status: { type: Number, required: true, default: 1 }, // 1: enviado, 2: Aceptado, 3: Preparando, 4: Listo para recoger, 5: entregado
  },
  { timestamps: true }
);

module.exports = mongoose.model("Order", OrderSchema);
