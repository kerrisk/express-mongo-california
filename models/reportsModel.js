var mongoose = require("mongoose");
const { ObjectID, Da } = require("mongoose/lib/schema/index");

var ReportModel = new mongoose.Schema(
  {
    idUsuario: { type: ObjectID, required: true },
    reportes: { type: Array, required: true },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Report", ReportModel);
