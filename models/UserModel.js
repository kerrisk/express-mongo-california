var mongoose = require("mongoose");

var UserSchema = new mongoose.Schema(
  {
    nombre: { type: String, required: true },
    apellidos: { type: String, required: true },
    calle: { type: String, required: false },
    estado: { type: String, required: false },
    municipio: { type: String, required: false },
    cp: { type: String, required: false },
    email: { type: String, required: true },
    password: { type: String, required: true },
    isConfirmed: { type: Boolean, required: true, default: 0 },
    confirmOTP: { type: String, required: false },
    otpTries: { type: Number, required: false, default: 0 },
    status: { type: Boolean, required: false, default: 1 },
    profilePic: {
      type: Object,
      required: false,
      default: { src: "public/uploads/default.png" },
    }, // [{imgsrc: src, imgname: name, uploaddate: date}]
    direcciones: { type: Array, required: false },
    isAdmin: { type: Boolean, required: true, default: false },
  },
  { timestamps: true }
);

// Virtual for user's full name
UserSchema.virtual("nombrecompleto").get(function () {
  return this.nombre + " " + this.apellidos;
});

module.exports = mongoose.model("User", UserSchema);
