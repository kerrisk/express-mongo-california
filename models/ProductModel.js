var mongoose = require("mongoose");
const { ObjectId } = require("mongoose/lib/schema/index");

var ProductSchema = new mongoose.Schema(
  {
    nombre: { type: String, required: true },
    descripcion: { type: String, required: false },
    precio: { type: Number, required: true },
    tipoVenta: { type: String, required: true }, // "kg" | "pza" | ...
    precioOferta: { type: Object, required: false }, // Puesto cuando {kg: 50} || {pza: 10}
    cantidad: { type: Number, required: true }, // Cantidad en inventario
    proveedor: {
      type: Object,
      required: false,
      default: "{nombre: Fruteria California, id: 02032",
    }, // [{provedor}] Obtenido del proveedor Model
    status: { type: Boolean, required: false, default: 1 }, // 1 disponible, 0 agotado
    categoria: { type: String, required: false, default: "fruta" }, // 1 disponible, 0 agotado
    picture: {
      type: Object,
      required: false,
      default: { src: "public/uploads/defaultProd.png" },
    }, // [{imgsrc: src, imgname: name, uploaddate: date}]
    minCompra: { type: Number, required: true },
  },
  { timestamps: true, shardkey: { categoria: 1 }});

module.exports = mongoose.model("Product", ProductSchema);
