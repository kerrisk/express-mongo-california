var mongoose = require("mongoose");

var ProveedorSchema = new mongoose.Schema(
  {
    id: { type: Number, required: true },
    nombre: { type: String, required: false },
  },
  { timestamps: true , shardkey: { _id: 1 }}
);

module.exports = mongoose.model("Proveedor", ProveedorSchema);