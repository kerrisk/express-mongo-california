const Order = require("../models/OrderModel");
const { validationResult, checkSchema } = require("express-validator");
const apiResponse = require("../helpers/apiResponse");
const auth = require("../middlewares/jwt");
//const authPrivileges = require("../middlewares/admin");
var mongoose = require("mongoose");
const { query } = require("express");
mongoose.set("useFindAndModify", false);

/**
 * Creates the model data for the product to be saved in mongo
 * @param {Object} req
 */
function orderModelData(data, user, empleado, isNew) {
  return {
    ...(isNew && {_id: user._id}),
    ...(data.Usuario && { idUsuario: data.Usuario._id }),
    ...(data.Usuario && { Usuario: data.Usuario }),
    ...(user && {idUsuario: user._id}),
    ...(user && {Usuario: user}),
    ...(empleado && { idEmpleado: empleado._id }),
    ...(empleado && { Empleado: empleado }),
    ...(data.Empleado && { idEmpleado: data.Empleado._id }),
    ...(data.Empleado && { Empleado: data.Empleado }),
    productos: data.productos,
    ...(data.fecha ? { fecha: data.fecha } : { fecha: new Date() }),
    total: data.total,
    status: data.status,
    ...(data.horaEstimada && { horaEstimada: data.horaEstimada }),
    ...(data.fechaEntrega && { fechaEntrega: data.fechaEntrega }),
  };
}
/**
 * Validation schema for the order store and update
 */
function orderSchemaValidation() {
  return {
    productos: {
      in: "body",
      isEmpty: {
        negated: true,
        errorMessage: "Se necesitan productos para crear la orden.",
      },
    },
    total: {
      in: "body",
      isEmpty: {
        negated: true,
        errorMessage: "Se el total para crear la orden.",
      },
      isFloat: {
        errorMessage: "El total debe ser numero.",
      },
      toFloat: true,
    },
  };
}

/**
 * Checks if id is valid
 */
function idSchemaUpdateDelete() {
  return {
    id: {
      in: ["body", "params"],
      custom: {
        options(value) {
          if (!mongoose.Types.ObjectId.isValid(value)) {
            this.message = "El id del producto no es valido";
            return false;
          }
          return true;
        },
        errorMessage: "El id del producto es erroneo.",
      },
    },
  };
}

/**
 * Order Store
 * Registers a new order
 */
exports.orderStore = [
  auth,
  //authPrivileges,
  checkSchema(orderSchemaValidation()),
  (req, res) => {
    try {
      console.log(req.body);
      console.log("---------------------------");
      console.log(req.user);
      const errors = validationResult(req);
      console.log(errors);
      if (!errors.isEmpty())
        return apiResponse.validationErrorWithData(
          res,
          "Error de validación de la orden.",
          errors.array()
        );
      var order = new Order(orderModelData(req.body, req.user));
      console.log(order);
      console.log("Validated Order Correctly");
      order.save(function (err) {
        if (err) {
          console.log("Error saving");
          return apiResponse.ErrorResponse(res, err);
        }
        let orderData = orderModelData(order, req.user, null, true);
        console.log("Success Mongo Save");
        return apiResponse.successResponseWithData(
          res,
          "Orden guardada correctamente.",
          orderData
        );
      });
    } catch (err) {
      //console.log(err);
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

/**
 * Order List
 *
 * @returns {Object}
 */
exports.orderList = [
  auth,
  function (req, res) {
    try {
      var query = {
        ...(req.user && {idUsuario: req.user._id})
      };
      Order.find().then((orders) => {
        if (orders.length > 0) {
          return apiResponse.successResponseWithData(
            res,
            "Consulta exitosa",
            orders
          );
        } else {
          return apiResponse.successResponseWithData(
            res,
            "Consulta exitosa pero no hay productos",
            []
          );
        }
      });
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

/**
 * Product Update.
 * Updates a product.
 */
exports.orderUpdate = [
  auth,
  //authPrivileges,
  //checkSchema(productSchemaUpdateValidation()),
  (req, res) => {
    try {
      console.log(req.body);
      const errors = validationResult(req);
      if (!errors.isEmpty())
        return apiResponse.validationErrorWithData(
          res,
          "Error de validación de la orden.",
          errors.array()
        );
      Order.findById(req.params.id, function (err, foundOrder) {
        if (foundOrder === null)
          return apiResponse.notFoundResponse(
            res,
            "Orden con el id especificado no ha sido encontrado."
          );
        Order.findByIdAndUpdate(
          req.params.id,
          orderModelData(req.body, null, req.user, false),
          { new: true },
          function (err, productUpdated) {
            if (err) return apiResponse.ErrorResponse(res, err);
            return apiResponse.successResponseWithData(
              res,
              "Orden modificada correctamente.",
              productUpdated
            );
          }
        );
      });
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

/**
 * Order Delete
 *
 * @returns {Object}
 */
exports.orderDelete = [
  checkSchema(idSchemaUpdateDelete()),
  (req, res) => {
    try {
      console.log(req.body);
      const errors = validationResult(req);
      console.log(errors);
      if (!errors.isEmpty())
        return apiResponse.validationErrorWithData(
          res,
          "Error de validación de la Orden.",
          errors.array()
        );
      Order.findById(req.params.id, function (err, foundOrder) {
        if (foundOrder === null)
          return apiResponse.notFoundResponse(
            res,
            "Orden con ese id no encontrado"
          );
        Order.findByIdAndRemove(req.params.id, function (err) {
          if (err) return apiResponse.ErrorResponse(res, err);
          return apiResponse.successResponse(
            res,
            "Orden eliminada correctamente"
          );
        });
      });
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  },
];
