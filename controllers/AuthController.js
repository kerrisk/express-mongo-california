const UserModel = require("../models/UserModel");
const { validationResult, checkSchema } = require("express-validator");
const auth = require("../middlewares/jwt");
//helper file to prepare responses.
const apiResponse = require("../helpers/apiResponse");
const utility = require("../helpers/utility");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const mailer = require("../helpers/mailer");
const { constants } = require("../helpers/constants");
var mongoose = require("mongoose");
const { has } = require("mongoose/lib/helpers/specialProperties");
/**
 * Creates the model data for the user to be saved in mongo or returned
 * @param {Object} user
 */
function UserData(user, file, hash, otp) {
  return {
    ...(user._id && { _id: user._id }),
    nombre: user.nombre,
    apellidos: user.apellidos,
    ...(user.calle && { calle: user.calle }),
    ...(user.estado && { estado: user.estado }),
    ...(user.municipio && { municipio: user.municipio }),
    ...(user.cp && { cp: user.cp }),
    email: user.email,
    ...(hash && { password: hash }),
    ...(otp && { confirmOTP: otp }),
    ...(user.status && { status: user.status }),
    ...(user.direcciones
      ? { direcciones: user.direcciones }
      : {
          direcciones: [
            {
              ...(user.calle && { calle: user.calle }),
              ...(user.estado && { estado: user.estado }),
              ...(user.municipio && { municipio: user.municipio }),
              ...(user.cp && { cp: user.cp }),
            },
          ],
        }),
    //if the image is from the mongo db
    ...(user.profilePic && { profilePic: user.profilePic }),
    ...(file && {
      profilePic: { src: file.path, originalname: file.originalname },
    }),
    isAdmin: user.isAdmin,
  };
}

/**
 * Validation schema for the user registration
 */
function userSchemaRegistrationValidation() {
  return {
    ...userSchemaValidation(),
    ...passwordRegisterLoginSchemaValidation(),
  };
}
/**
 * Validation schema for the user login
 */
function userSchemaLoginValidation() {
  return {
    ...emailSchemaValidation(),
    ...passwordRegisterLoginSchemaValidation(),
  };
}
/**
 * Validation schema for the user update data
 */
function userSchemaUpdataValidation() {
  return { ...userSchemaValidation(), ...passwordUpdateSchemaValidation(), ...emailSchemaValidation() };
}
/**
 * Validation schema for the user otp validation
 */
function userSchemaOtpValidation() {
  return { ...emailSchemaValidation(), ...otpSchemaValidation() };
}
/**
 * General Validation for user data
 */
function userSchemaValidation() {
  return {
    id: {
      in: ["body", "param"],
      optional: { options: { nullable: true } },
      custom: {
        options(value) {
          if (!mongoose.Types.ObjectId.isValid(value)) {
            this.message = "El id del usuario no es valido";
            return false;
          }
          return true;
        },
        errorMessage: "El id del usuario es erroneo.",
      },
      customSanitizer: {
        options: (value) => mongoose.Types.ObjectId(value),
      },
    },
    nombre: {
      in: "body",
      trim: true,
      escape: true,
      isEmpty: {
        negated: true,
        errorMessage: "El nombre es requerido.",
      },
    },
    apellidos: {
      in: "body",
      trim: true,
      escape: true,
      isEmpty: {
        negated: true,
        errorMessage: "El apellido es requerido.",
      },
    },
    ...emailRegisterSchemaValidation(),
    calle: {
      in: "body",
      optional: { options: { nullable: true } },
      trim: true,
      escape: true,
    },
    estado: {
      in: "body",
      optional: { options: { nullable: true } },
      trim: true,
      escape: true,
    },
    municipio: {
      in: "body",
      optional: { options: { nullable: true } },
      trim: true,
      escape: true,
    },
    cp: {
      in: "body",
      optional: { options: { nullable: true } },
      trim: true,
      escape: true,
    },
    calle: {
      in: "body",
      optional: { options: { nullable: true } },
      trim: true,
      escape: true,
    },
  };
}
/**
 * Validation schema helpers
 */
function emailSchemaValidation() {
  return {
    email: {
      in: ["body", "params"],
      trim: true,
      escape: true,
      isEmpty: {
        negated: true,
        errorMessage: "El email es requerido.",
      },
      isEmail: {
        errorMessage: "El email no es valido",
      },
    },
  };
}
function emailRegisterSchemaValidation() {
  return {
    email: {
      in: ["body", "params"],
      trim: true,
      escape: true,
      isEmpty: {
        negated: true,
        errorMessage: "El email es requerido.",
      },
      custom: {
        options(value) {
          return new Promise(function (resolve, reject){
            UserModel.findOne({email: value}).then(user => {
              console.log(user)
              if (user) {
                reject("Email ya esta en uso");
              }
              resolve()
            })
          })
        },
        errorMessage: "El email ya esta en uso",
      },
      isEmail: {
        errorMessage: "El email no es valido",
      },
    },
  };
}
/**
 * The password is requiered to register and login
 */
function passwordRegisterLoginSchemaValidation() {
  return {
    password: {
      in: "body",
      trim: true,
      escape: true,
      isLength: {
        options: { min: 6 },
        errorMessage: "La contraseña debe tener mínimo 6 caracteres.",
      },
    },
  };
}
/**
 * The password could be omitted when updatting user info
 */
function passwordUpdateSchemaValidation() {
  return {
    password: {
      in: "body",
      optional: { options: { nullable: true } },
      trim: true,
      escape: true,
      isLength: {
        options: { min: 6 },
        errorMessage: "La contraseña debe tener mínimo 6 caracteres.",
      },
    },
  };
}
/**
 * The password could be omitted when updatting user info
 */
function otpSchemaValidation() {
  return {
    otp: {
      in: ["body", "params"],
      trim: true,
      escape: true,
      isLength: {
        options: { min: 1 },
        errorMessage:
          "El codigo de confirmacion otp debe ser por lo menos de un caracter.",
      },
    },
  };
}
/**
 *
 */
function emailConfirmMessage(email, otp) {
  return (
    "<p>Please Confirm your Account. Using the following link</p><a href='" +
    process.env.ROOT_API +
    "/auth/verify-otp/" +
    email +
    "/" +
    otp +
    "'>confirm account</a><p>Or if the link does not work copy and paste the following address in your browser</p>\
  <p>" +
    process.env.ROOT_API +
    "/auth/verify-otp/" +
    email +
    "/" +
    otp +
    "</p>"
  );
}

/**
 * User registration.
 *
 * @param {string}      firstName
 * @param {string}      lastName
 * @param {string}      email
 * @param {string}      password
 *
 * @returns {Object}
 */
exports.register = [
  // Validate fields.
  checkSchema(userSchemaRegistrationValidation()),
  // Process request after validation and sanitization.
  (req, res) => {
    try {
      console.log(req.body);
      console.log(userSchemaRegistrationValidation());
      // Extract the validation errors from a request.
      const errors = validationResult(req);
      if (!errors.isEmpty())
        // Display sanitized values/errors messages.
        return apiResponse.validationErrorWithData(
          res,
          "Validation Error.",
          errors.array()
        );
      //hash input password
      bcrypt.hash(req.body.password, 10, function (err, hash) {
        // generate OTP for confirmation
        let otp = utility.randomNumber(4);
        // Create User object with escaped and trimmed data
        var user = new UserModel(UserData(req.body, req.file, hash, otp));
        // Html email body
        let html = emailConfirmMessage(user.email, user.confirmOTP);
        // Send confirmation email
        mailer
          .send(
            constants.confirmEmails.from,
            req.body.email,
            "Confirm Account",
            html
          )
          .then(function () {
            // Save user.
            user.save(function (err) {
              if (err) {
                console.error(err);
                return apiResponse.ErrorResponse(res, err);
              }
              let userData = UserData(user);
              return apiResponse.successResponseWithData(
                res,
                "Registro Existoso.",
                userData
              );
            });
          })
          .catch((err) => {
            console.log(err);
            return apiResponse.ErrorResponse(res, err);
          });
      });
    } catch (err) {
      console.error(err);
      //throw error in json response with status 500.
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

/**
 * User login.
 *
 * @param {string}      email
 * @param {string}      password
 *
 * @returns {Object}
 */
exports.login = [
  checkSchema(userSchemaLoginValidation()),
  (req, res) => {
    try {
      const errors = validationResult(req);
      //If any validation errors ocurred, responds with a request error and sends the errors as an array.
      if (!errors.isEmpty())
        return apiResponse.validationErrorWithData(
          res,
          "Validation Error.",
          errors.array()
        );
      //Retrieves the user from the database.
      UserModel.findOne({ email: req.body.email }).then((user) => {
        if (!user)
          return apiResponse.unauthorizedResponse(
            res,
            "Email or Password wrong."
          );
        //Compare given password with db's hash.
        bcrypt.compare(req.body.password, user.password, function (err, same) {
          if (!same)
            return apiResponse.unauthorizedResponse(
              res,
              "Email or Password wrong."
            );
          //Check account confirmation.
          if (!user.isConfirmed)
            return apiResponse.unauthorizedResponse(
              res,
              "Account is not confirmed. Please confirm your account."
            );
          // Check User's account active or not.
          if (!user.status)
            return apiResponse.unauthorizedResponse(
              res,
              "Account is not active. Please contact admin."
            );
          let userData = UserData(user);
          console.log(userData);
          //Prepare JWT token for authentication
          const jwtPayload = userData;
          console.log(jwtPayload);
          const jwtData = {
            expiresIn: process.env.JWT_TIMEOUT_DURATION,
          };
          const secret = process.env.JWT_SECRET;
          //Generated JWT token with Payload and secret.
          userData.token = jwt.sign(jwtPayload, secret, jwtData);
          return apiResponse.successResponseWithData(
            res,
            "Login Success.",
            userData
          );
        });
      });
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

/**
 * Verify Confirm otp.
 *
 * @param {string}      email
 * @param {string}      otp
 *
 * @returns {Object}
 */
exports.verifyConfirm = [
  checkSchema(userSchemaOtpValidation()),
  (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty())
        return apiResponse.validationErrorWithData(
          res,
          "Validation Error.",
          errors.array()
        );
      var query = { email: req.params.email };
      UserModel.findOne(query).then((user) => {
        if (!user)
          return apiResponse.unauthorizedResponse(
            res,
            "Specified email not found."
          );
        //Check already confirm or not.
        if (user.isConfirmed)
          return apiResponse.unauthorizedResponse(
            res,
            "Account already confirmed."
          );
        //Check account confirmation.
        if (!user.confirmOTP == req.params.otp)
          return apiResponse.unauthorizedResponse(res, "Otp does not match");
        //Update user as confirmed
        UserModel.findOneAndUpdate(query, {
          isConfirmed: 1,
          confirmOTP: null,
        }).catch((err) => {
          return apiResponse.ErrorResponse(res, err);
        });
        return apiResponse.successResponse(res, "Account confirmed success.");
      });
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

/**
 * Resend Confirm otp.
 *
 * @param {string}      email
 *
 * @returns {Object}
 */
exports.resendConfirmOtp = [
  checkSchema(emailSchemaValidation()),
  (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return apiResponse.validationErrorWithData(
          res,
          "Validation Error.",
          errors.array()
        );
      } else {
        var query = { email: req.body.email };
        UserModel.findOne(query).then((user) => {
          if (user) {
            //Check already confirm or not.
            if (!user.isConfirmed) {
              // Generate otp
              let otp = utility.randomNumber(4);
              // Html email body
              let html = emailConfirmMessage(user.email, user.confirmOTP);
              // Send confirmation email
              mailer
                .send(
                  constants.confirmEmails.from,
                  req.body.email,
                  "Confirm Account",
                  html
                )
                .then(function () {
                  user.isConfirmed = 0;
                  user.confirmOTP = otp;
                  // Save user.
                  user.save(function (err) {
                    if (err) {
                      return apiResponse.ErrorResponse(res, err);
                    }
                    return apiResponse.successResponse(
                      res,
                      "Confirm otp sent."
                    );
                  });
                });
            } else {
              return apiResponse.unauthorizedResponse(
                res,
                "Account already confirmed."
              );
            }
          } else {
            return apiResponse.unauthorizedResponse(
              res,
              "Specified email not found."
            );
          }
        });
      }
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

/**
 * User update profile info.
 *
 * @param {string}      firstName
 * @param {string}      lastName
 * @param {string}      email
 * @param {string}      password
 *
 * @returns {Object}
 */
exports.update = [
  auth, //Checks the jwt Token
  // Validate fields.
  checkSchema(userSchemaUpdataValidation()),
  // Process request after validation and sanitization.
  (req, res) => {
    try {
      console.log(req.body);
      // Extract the validation errors from a request.
      const errors = validationResult(req);
      if (!errors.isEmpty())
        // Display sanitized values/errors messages.
        return apiResponse.validationErrorWithData(
          res,
          "Validation Error.",
          errors.array()
        );
      //hash input password
      bcrypt.hash(req.body.password, 10, function (err, hash) {
        // generate OTP for confirmation
        let otp = utility.randomNumber(4);
        hash = req.body.password ? hash : null;
        // Create User data to update with escaped and trimmed data
        const update = UserData(req.body, req.file, hash);
        console.log(update);
        UserModel.findByIdAndUpdate(
          req.user._id,
          update,
          { new: true },
          function (err, user) {
            console.log(user);
            if (err) {
              console.error(err);
              return apiResponse.ErrorResponse(
                res,
                "Ocurrio un error actualizando el usuario"
              );
            }
            let userData = UserData(user);
            return apiResponse.successResponseWithData(
              res,
              "Actualización exitosa.",
              userData
            );
          }
        );
      });
    } catch (err) {
      console.error(err);
      //throw error in json response with status 500.
      return apiResponse.ErrorResponse(res, err);
    }
  },
];


/**
 * User deletes profile.
 *
 *
 * @returns {Object}
 */
exports.deleteAccount = [
  auth, //Checks the jwt Token
  // Validate fields.
  // Process request after validation and sanitization.
  (req, res) => {
    try {
      console.log(req.user);
      // Extract the validation errors from a request.
      const errors = validationResult(req);
      if (!errors.isEmpty())
        // Display sanitized values/errors messages.
        return apiResponse.validationErrorWithData(
          res,
          "Validation Error.",
          errors.array()
        );
      //hash input password
      bcrypt.hash(req.body.password, 10, function (err, hash) {
        // generate OTP for confirmation
        let otp = utility.randomNumber(4);
        hash = req.body.password ? hash : null;
        // Create User data to update with escaped and trimmed data
        const update = UserData(req.body, req.file, hash);
        console.log(update);
        UserModel.findByIdAndDelete(
          req.user._id,
          function (err, user) {
            console.log(user);
            if (err) {
              console.error(err);
              return apiResponse.ErrorResponse(
                res,
                "Ocurrio un error eliminando el usuario"
              );
            }
            let userData = UserData(user);
            return apiResponse.successResponseWithData(
              res,
              "Usuario eliminado.",
              userData
            );
          }
        );
      });
    } catch (err) {
      console.error(err);
      //throw error in json response with status 500.
      return apiResponse.ErrorResponse(res, err);
    }
  },
];
