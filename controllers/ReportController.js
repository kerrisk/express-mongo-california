const Order = require("../models/OrderModel");
const apiResponse = require("../helpers/apiResponse");
const auth = require("../middlewares/jwt");
//const authPrivileges = require("../middlewares/admin");
var mongoose = require("mongoose");
mongoose.set("useFindAndModify", false);

exports.ProductosVenta = [
  auth,
  function (req, res) {
    var results = [];
    try {
      var o = {};
      o.query = {status: 5};
      o.map = function() {
        for (var idx = 0; idx < this.productos.length; idx++){
         var key = this.productos[idx].nombre;
         var value = this.productos[idx].precio * this.productos[idx].cantidad;
         emit(key, value);
        }
      };
      o.reduce = function(key, values){
        return Array.sum([...values]);
      };
      console.log(o)
      Order.mapReduce(o, function(err, result) {
        console.log("After MapREduce")
        if (err){
          console.log("Error");
          console.log(err.stats);
          return apiResponse.ErrorResponse(res, err);
        }
        console.log("Fine");
        console.log(result);
        apiResponse.successResponseWithData(res, "Reportes ProductosVenta generado correctamente", result);
      });
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  },
];
exports.ProductosVentaDB = [
  auth,
  function (req, res) {
    var results = [];
    try {
      var o = {};
      o.query = {status: 5};
      o.map = function() {
        for (var idx = 0; idx < this.productos.length; idx++){
         var key = this.productos[idx].nombre;
         var value = this.productos[idx].precio * this.productos[idx].cantidad;
         emit(key, value);
        }
      };
      o.out ={merge: "reportes-productos-venta"};
      o.reduce = function(key, values){
        return Array.sum([...values]);
      };
      console.log(o)
      Order.mapReduce(o, function(err, result) {
        console.log("After MapREduce")
        if (err){
          console.log("Error");
          return apiResponse.ErrorResponse(res, "Error al ejecutar el reporte ProductoVentaDB");
        }
        console.log("Fine");
        return apiResponse.successResponse(res, "Reportes ProductosVenta generado correctamente y guardados en la db");
      });
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

exports.ProductosCantidad = [
  auth,
  function (req, res) {
    var results = [];
    try {
      var o = {};
      o.query = {status: 5};
      o.map = function() {
        for (var idx = 0; idx < this.productos.length; idx++){
         var key = this.productos[idx].nombre;
         var value = this.productos[idx].cantidad;
         emit(key, value);
        }
      };
      o.reduce = function(key, values){
        return Array.sum([...values]);
      };
      console.log(o)
      Order.mapReduce(o, function(err, result) {
        console.log("After MapREduce")
        if (err){
          console.log("Error");
          console.log(err.stats);
          return apiResponse.ErrorResponse(res, err);
        }
        console.log("Fine");
        console.log(result);
        apiResponse.successResponseWithData(res, "Reporte ProductosCantidad generado correctamente", result);
      });
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

exports.ProductosCantidadDB = [
  auth,
  function (req, res) {
    var results = [];
    try {
      var o = {};
      o.query = {status: 5};
      o.out ={merge: "reportes-productos-cantidad"};
      o.map = function() {
        for (var idx = 0; idx < this.productos.length; idx++){
         var key = this.productos[idx].nombre;
         var value = this.productos[idx].cantidad;
         emit(key, value);
        }
      };
      o.reduce = function(key, values){
        return Array.sum([...values]);
      };
      console.log(o)
      Order.mapReduce(o, function(err, result) {
        console.log("After MapREduce")
        if (err){
          console.log("Error");
          return apiResponse.ErrorResponse(res, "Error al ejecutar el reporte ProductoCantidadDB");
        }
        console.log("Fine");
        apiResponse.successResponse(res, "Reporte ProductosCantidadDB generado correctamente y guardados en la db");
      });
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

exports.Empleados = [
  auth,
  function (req, res) {
    var results = [];
    try {
      var o = {};
      o.query = {status: 5};
      o.map = function() {
        if (this.Empleado){
          var key = this.Empleado.nombre;
          var value = 1;
          emit(key, value);
        }
      };
      o.reduce = function(key, values){
        return Array.sum([...values]);
      };
      console.log(o)
      Order.mapReduce(o, function(err, result) {
        console.log("After MapREduce")
        if (err){
          console.log("Error");
          console.log(err.stats);
          return apiResponse.ErrorResponse(res, err);
        }
        console.log("Fine");
        console.log(result);
        apiResponse.successResponseWithData(res, "Reporte Empleados generados correctamente", result);
      });
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

exports.EmpleadosDB = [
  auth,
  function (req, res) {
    var results = [];
    try {
      var o = {};
      o.query = {status: 5};
      o.out ={merge: "reportes-empleados"};
      o.map = function() {
         var key = this.Empleado.nombre;
         var value = 1;
         emit(key, value);
      };
      o.reduce = function(key, values){
        return Array.sum([...values]);
      };
      console.log(o)
      Order.mapReduce(o, function(err, result) {
        console.log("After MapREduce")
        if (err){
          console.log("Error");
          return apiResponse.ErrorResponse(res, "Error al ejecutar el reporte EmpleadosDB");
        }
        console.log("Fine");
        apiResponse.successResponseWithData(res, "Reporte Empleados generado correctamente y guardado a la db");
      });
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  },
];