const Proveedor = require("../models/ProveedorModel");
const apiResponse = require("../helpers/apiResponse");
const auth = require("../middlewares/jwt");
//const authPrivileges = require("../middlewares/admin");
var mongoose = require("mongoose");
const { query } = require("express");
mongoose.set("useFindAndModify", false);

/**
 * Order List
 *
 * @returns {Object}
 */
exports.proveedorList = [
  function (req, res) {
    try {
      var prov = new Proveedor({id: 7, nombre: "Luis"});
      prov.save(function (err) {
        if (err) {
          console.log("Error saving");
        }
        console.log(prov)
      });
      Proveedor.find().then((proveedores) => {
        if (proveedores.length > 0) {
          return apiResponse.successResponseWithData(
            res,
            "Consulta exitosa",
            proveedores
          );
        } else {
          return apiResponse.successResponseWithData(
            res,
            "Consulta exitosa pero no hay productos",
            []
          );
        }
      });
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  },
];
