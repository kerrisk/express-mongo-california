const Cart = require("../models/CartModel");
const { validationResult, checkSchema } = require("express-validator");
const apiResponse = require("../helpers/apiResponse");
const auth = require("../middlewares/jwt");
//const authPrivileges = require("../middlewares/admin");
var mongoose = require("mongoose");
const { query } = require("express");
mongoose.set("useFindAndModify", false);

/**
 * Creates the model data for the product to be saved in mongo
 * @param {Object} req
 */
function cartModelData(data, user) {
  return {
    ...(user && {idUsuario: user._id}),
    ...(data.idUsuario && {idUsuario: data.idUsuario}),
    //...(data._id && {_id: data._id}),
    productos: data.productos,
  };
}
/**
 * Validation schema for the order store and update
 */
function cartSchemaValidation() {
  return {
    productos: {
      in: "body",
      isEmpty: {
        negated: true,
        errorMessage: "Se necesitan productos para crear la orden.",
      },
    },
  };
}


/**
 * Cart Store
 * Registers a new order
 */
exports.cartStore = [
  auth,
  //authPrivileges,
  (req, res) => {
    try {
      console.log(req.body);
      console.log("--------------Cart Store-------------");
      console.log(req.user);
      const errors = validationResult(req);
      console.log(errors);
      if (!errors.isEmpty())
        return apiResponse.validationErrorWithData(
          res,
          "Error de validación de la orden.",
          errors.array()
        );
      Cart.findOneAndUpdate({idUsuario: req.user._id}, cartModelData(req.body, req.user), {new: true}, function (err, foundOrder) {
        if (err) return apiResponse.ErrorResponse(res, err);
        if (foundOrder === null){
          const cart = new Cart(cartModelData(req.body, req.user));
          cart.save(function (err){
            if (err) {
              console.log("Error creating cart");
              return apiResponse.ErrorResponse(res, err);
            }
            console.log("Success Mongo Save");
            return apiResponse.successResponseWithData(
              res,
              "Cart guardado correctamente.",
              cart
            );
          })
        }
      });
    } catch (err) {
      //console.log(err);
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

/**
 * Order List
 *
 * @returns {Object}
 */
exports.cartDetail = [
  auth,
  function (req, res) {
    try {
      console.log(req.body);
      console.log(req.user);
      var query = {idUsuario: req.user._id};
      Cart.findOne(query).then((cart) => {
        if (cart === null){
          return apiResponse.successResponseWithData(
            res,
            "Consulta exitosa pero no hay productos",
            {productos: []}
          );
        }
        return apiResponse.successResponseWithData(
          res,
          "Consulta cart exitosa",
          cart
        );
      });
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

/**
 * Product Update.
 * Updates a product.
 */
exports.orderUpdate = [
  auth,
  //authPrivileges,
  //checkSchema(productSchemaUpdateValidation()),
  (req, res) => {
    try {
      console.log(req.body);
      const errors = validationResult(req);
      if (!errors.isEmpty())
        return apiResponse.validationErrorWithData(
          res,
          "Error de validación de la orden.",
          errors.array()
        );
      Order.findById(req.params.id, function (err, foundOrder) {
        if (foundOrder === null)
          return apiResponse.notFoundResponse(
            res,
            "Orden con el id especificado no ha sido encontrado."
          );
        Order.findByIdAndUpdate(
          req.params.id,
          orderModelData(req.body, null, req.user),
          { new: true },
          function (err, productUpdated) {
            if (err) return apiResponse.ErrorResponse(res, err);
            let orderData = orderModelData(productUpdated);
            return apiResponse.successResponseWithData(
              res,
              "Orden modificada correctamente.",
              orderData
            );
          }
        );
      });
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

/**
 * Order Delete
 *
 * @returns {Object}
 */
exports.orderDelete = [
  (req, res) => {
    try {
      console.log(req.body);
      const errors = validationResult(req);
      console.log(errors);
      if (!errors.isEmpty())
        return apiResponse.validationErrorWithData(
          res,
          "Error de validación de la Orden.",
          errors.array()
        );
      Order.findById(req.params.id, function (err, foundOrder) {
        if (foundOrder === null)
          return apiResponse.notFoundResponse(
            res,
            "Orden con ese id no encontrado"
          );
        Order.findByIdAndRemove(req.params.id, function (err) {
          if (err) return apiResponse.ErrorResponse(res, err);
          return apiResponse.successResponse(
            res,
            "Orden eliminada correctamente"
          );
        });
      });
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  },
];
