const Product = require("../models/ProductModel");
const { validationResult, checkSchema } = require("express-validator");
const apiResponse = require("../helpers/apiResponse");
const auth = require("../middlewares/jwt");
//const authPrivileges = require("../middlewares/admin");
var mongoose = require("mongoose");
mongoose.set("useFindAndModify", false);

/**
 * Creates the model data for the product to be saved in mongo
 * @param {Object} req
 */
function productModelData(data, file) {
  return {
    ...(data.id && { _id: data.id }),
    nombre: data.nombre,
    descripcion: data.descripcion,
    precio: parseFloat(data.precio),
    tipoVenta: data.tipoVenta,
    ...(data.precioOferta && { precioOferta: data.precioOferta }),
    cantidad: parseFloat(data.cantidad),
    ...(data.proveedor && { proveedor: data.proveedor }),
    ...(data.status && { status: data.status }),
    ...(data.categoria && { categoria: data.categoria }),
    // if the file exists it should return the object with the image co
    ...(file && {
      picture: { src: file.path, originalname: file.originalname },
    }),
    // If the picture is in the data it should return it.
    ...(data.picture && {
      picture: data.picture,
    }),
    ...(data.minCompra && { minCompra: data.minCompra }),
  };
}
/**
 * Validation schema for the product store and update
 */
function productSchemaValidation() {
  return {
    nombre: {
      in: "body",
      trim: true,
      escape: true,
      isEmpty: {
        negated: true,
        errorMessage: "El nombre del producto es requerido.",
      },
    },
    descripcion: {
      in: "body",
      trim: true,
      escape: true,
      isEmpty: {
        negated: true,
        errorMessage: "La descripcion del producto es requerida.",
      },
    },
    precio: {
      in: "body",
      trim: true,
      escape: true,
      isEmpty: {
        negated: true,
        errorMessage: "El precio del producto es requerido.",
      },
      isFloat: {
        errorMessage: "El precio debe ser un numero.",
      },
      toFloat: true,
    },
    tipoVenta: {
      in: "body",
      trim: true,
      escape: true,
      isEmpty: {
        negated: true,
        errorMessage: "El tipo de venta del producto es requerido.",
      },
      isIn: {
        options: [["kg", "pza"]],
        errorMessage: "El tipo de venta debe ser kg o pza.",
      },
    },
    precioOferta: {
      optional: { options: { nullable: true } },
      trim: true,
      escape: true,
      isFloat: {
        errorMessage: "El precio oferta debe ser un numero.",
      },
      toFloat: true,
    },
    cantidad: {
      in: "body",
      trim: true,
      escape: true,
      isEmpty: {
        negated: true,
        errorMessage: "La cantidad del producto es requerida.",
      },
      isFloat: {
        errorMessage: "La cantidad debe ser un numero.",
      },
      toFloat: true,
    },
    proveedor: {
      in: "body",
      optional: { options: { nullable: true } },
      custom: {
        options(value) {
          try {
            JSON.parse(value);
          } catch (err) {
            this.message = "El proveedor no es valido";
            return false;
          } finally {
            return true;
          }
        },
        errorMessage: "El proveedor no es valido.",
      },
      customSanitizer: {
        options: (value) => JSON.parse(value),
      },
    },
    "proveedor.id": {
      in: "body",
      isInt: {
        errorMessage: "El id del proveedor debe ser entero",
      },
      toInt: true,
    },
    "proveedor.nombre": {
      in: "body",
      trim: true,
      escape: true,
      isEmpty: {
        negated: true,
        errorMessage: "El nombre del proveedor es requrido",
      },
    },
    status: {
      in: "body",
      optional: { options: { nullable: true } },
      isInt: {
        errorMessage: "El status debe ser un numero entero",
      },
      toInt: true,
    },
    categoria: {
      in: "body",
      trim: true,
      escape: true,
      isEmpty: {
        negated: true,
        errorMessage: "La categoria del producto es requerido.",
      },
      isIn: {
        options: [["fruta", "verdura", "cereal", "otro"]],
        errorMessage: "La categoria debe de ser fruta, verdura, cereal u otro.",
      },
    },
    minCompra: {
      trim: true,
      escape: true,
      isEmpty: {
        negated: true,
        errorMessage: "La cantidad minima de compra es requerida",
      },
      isFloat: {
        errorMessage: "La cantidad minima de compra debe ser un numero",
      },
    },
  };
}

function productSchemaUpdateValidation() {
  return { ...productSchemaValidation, ...idSchemaUpdateDelete };
}

function idSchemaUpdateDelete() {
  return {
    id: {
      in: ["body", "params"],
      custom: {
        options(value) {
          if (!mongoose.Types.ObjectId.isValid(value)) {
            this.message = "El id del producto no es valido";
            return false;
          }
          return true;
        },
        errorMessage: "El id del producto es erroneo.",
      },
    },
  };
}
/**
 * Product List
 *
 * @returns {Object}
 */
exports.productList = [
  function (req, res) {
    try {
      Product.find().then((products) => {
        if (products.length > 0) {
          return apiResponse.successResponseWithData(
            res,
            "Consulta exitosa",
            products
          );
        } else {
          return apiResponse.successResponseWithData(
            res,
            "Consulta exitosa pero no hay productos",
            []
          );
        }
      });
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

/**
 * Product Store
 * Registers a new product
 */
exports.productStore = [
  auth,
  //authPrivileges,
  checkSchema(productSchemaValidation()),
  (req, res) => {
    try {
      console.log(req.body);
      const errors = validationResult(req);
      console.log(errors);
      if (!errors.isEmpty())
        return apiResponse.validationErrorWithData(
          res,
          "Error de validación del producto.",
          errors.array()
        );
      var producto = new Product(productModelData(req.body, req.file));
      console.log("Validated Product Correctly");
      producto.save(function (err) {
        if (err) {
          console.log("Error saving");
          return apiResponse.ErrorResponse(res, err);
        }
        let productoData = productModelData(producto);
        console.log("Success Mongo Save");
        return apiResponse.successResponseWithData(
          res,
          "Producto registrado correctamente.",
          productoData
        );
      });
    } catch (err) {
      //console.log(err);
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

/**
 * Product Update.
 * Updates a product.
 */
exports.productUpdate = [
  auth,
  //authPrivileges,
  checkSchema(productSchemaUpdateValidation()),
  (req, res) => {
    try {
      console.log(req.body);
      const errors = validationResult(req);
      if (!errors.isEmpty())
        return apiResponse.validationErrorWithData(
          res,
          "Error de validación del producto.",
          errors.array()
        );
      Product.findById(req.params.id, function (err, foundProduct) {
        if (foundProduct === null)
          return apiResponse.notFoundResponse(
            res,
            "Producto con el id especificado no ha sido encontrado."
          );
        console.log(foundProduct);
        Product.findByIdAndUpdate(
          req.params.id,
          productModelData(req.body, req.file),
          { new: true },
          function (err, productUpdated) {
            if (err) return apiResponse.ErrorResponse(res, err);
            let productoData = productModelData(productUpdated);
            return apiResponse.successResponseWithData(
              res,
              "Producto modificado correctamente.",
              productoData
            );
          }
        );
      });
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

/**
 * Product Delete
 *
 * @returns {Object}
 */
exports.productDelete = [
  checkSchema(idSchemaUpdateDelete()),
  (req, res) => {
    try {
      console.log(req.body);
      const errors = validationResult(req);
      console.log(errors);
      if (!errors.isEmpty())
        return apiResponse.validationErrorWithData(
          res,
          "Error de validación del producto.",
          errors.array()
        );
      Product.findById(req.params.id, function (err, foundProduct) {
        if (foundProduct === null)
          return apiResponse.notFoundResponse(
            res,
            "Producto con ese id no encontrado"
          );
        Product.findByIdAndRemove(req.params.id, function (err) {
          if (err) return apiResponse.ErrorResponse(res, err);
          return apiResponse.successResponse(
            res,
            "Libro eliminado correctamente"
          );
        });
      });
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  },
];
