var express = require("express");
var authRouter = require("./auth");
var bookRouter = require("./book");
var productRouter = require("./product");
var orderRouter = require("./order");
var proveedorRouter = require("./proveedor");
var cartRouter = require("./cart");
var reportRouter = require("./report");

var app = express();

app.use("/auth/", authRouter);
app.use("/book/", bookRouter);
app.use("/product/", productRouter);
app.use("/order/", orderRouter);
app.use("/proveedor/", proveedorRouter);
app.use("/cart/", cartRouter);
app.use("/report/", reportRouter);

module.exports = app;
