var express = require("express");
const ProductController = require("../controllers/ProductController");
var multer = require("multer");
var upload = multer({ dest: "./public/uploads/" });
var router = express.Router();

router.get("/", ProductController.productList);
// router.get("/:id", BookController.bookDetail);
router.post("/", upload.single("picture"), ProductController.productStore);
router.put("/:id", upload.single("picture"), ProductController.productUpdate);
router.delete("/:id", ProductController.productDelete);

module.exports = router;
