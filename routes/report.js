var express = require("express");
const ReportController = require("../controllers/ReportController");
var router = express.Router();

//router.get("/", CartController.orderList);
router.get("/ProductosVenta", ReportController.ProductosVenta);
router.get("/ProductosCantidad", ReportController.ProductosCantidad);
router.get("/Empleados", ReportController.Empleados);
router.get("/ProductosVentaDB", ReportController.ProductosVentaDB);
router.get("/ProductosCantidadDB", ReportController.ProductosCantidadDB);
 router.get("/EmpleadosDB", ReportController.EmpleadosDB);
//router.post("/", CartController.cartStore);
//router.put("/:id", CartController.cartUpdate);
//router.delete("/:id", CartController.orderDelete);

module.exports = router;
