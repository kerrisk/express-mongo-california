var express = require("express");
const AuthController = require("../controllers/AuthController");
var multer = require("multer");
var upload = multer({ dest: "./public/uploads/" });
var router = express.Router();

router.post("/register", upload.single("profile"), AuthController.register);
router.post("/update", upload.single("profile"), AuthController.update);
router.post("/login", AuthController.login);
router.get("/verify-otp/:email/:otp", AuthController.verifyConfirm);
router.post("/resend-verify-otp", AuthController.resendConfirmOtp);
router.delete("/", AuthController.deleteAccount);

module.exports = router;
