var express = require("express");
const CartController = require("../controllers/CartController");
var router = express.Router();

//router.get("/", CartController.orderList);
router.get("/", CartController.cartDetail);
router.post("/", CartController.cartStore);
//router.put("/:id", CartController.cartUpdate);
//router.delete("/:id", CartController.orderDelete);

module.exports = router;
