var express = require("express");
const ProveedorController = require("../controllers/ProveedorController");
var router = express.Router();

router.get("/", ProveedorController.proveedorList);
//router.get("/:id", BookController.bookDetail);
//router.post("/", OrderController.orderStore);
//router.put("/:id", upload.single("picture"), ProductController.productUpdate);
//router.delete("/:id", OrderController.orderDelete);

module.exports = router;
