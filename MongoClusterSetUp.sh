#!/bin/bash 
tmux new-session \; \
    split-window -h -p 75 \; \
    split-window -h -p 67 \; \
    split-window -h -p 50 \; \
    split-window -v -p 67 \; \
    split-window -v -p 50 \; \
    select-pane -t 2 \; \
    split-window -v -p 67 \; \
    split-window -v -p 50 \; \
    select-pane -t 1 \; \
    split-window -v -p 67 \; \
    split-window -v -p 50 \; \
    select-pane -t 0 \; \
    split-window -v -p 67 \; \
    split-window -v -p 50 \; \
    select-pane -t 0 \; \
    send-keys 'mongod --configsvr --replSet ReplicaSetConfig --dbpath /var/lib/cfgsvr1 --port 27018 --logpath /var/lib/cfgsvr1/log --logappend' C-m \; \
    select-pane -t 1 \; \
    send-keys 'mongod --configsvr --replSet ReplicaSetConfig --dbpath /var/lib/cfgsvr2 --port 27019 --logpath /var/lib/cfgsvr2/log --logappend' C-m \; \
    select-pane -t 2 \; \
    send-keys 'mongod --configsvr --replSet ReplicaSetConfig --dbpath /var/lib/cfgsvr3 --port 27020 --logpath /var/lib/cfgsvr3/log --logappend' C-m \; \
    select-pane -t 3 \; \
    send-keys 'mongod --shardsvr --replSet ReplicaSet1 --dbpath /var/lib/replica11 --logpath /var/lib/replica11/log --port 27011 --logappend --smallfiles --oplogSize 50 --nojournal' C-m \; \
    select-pane -t 4 \; \
    send-keys 'mongod --shardsvr --replSet ReplicaSet1 --dbpath /var/lib/replica12 --logpath /var/lib/replica12/log --port 27012 --logappend --smallfiles --oplogSize 50 --nojournal' C-m \; \
    select-pane -t 5 \; \
    send-keys 'mongod --shardsvr --replSet ReplicaSet1 --dbpath /var/lib/replica13 --logpath /var/lib/replica13/log --port 27013 --logappend --smallfiles --oplogSize 50 --nojournal' C-m \; \
    select-pane -t 6 \; \
    send-keys 'mongod --shardsvr --replSet ReplicaSet2 --dbpath /var/lib/replica21 --logpath /var/lib/replica21/log --port 27021 --logappend --smallfiles --oplogSize 50 --nojournal' C-m \; \
    select-pane -t 7 \; \
    send-keys 'mongod --shardsvr --replSet ReplicaSet2 --dbpath /var/lib/replica22 --logpath /var/lib/replica22/log --port 27022 --logappend --smallfiles --oplogSize 50 --nojournal' C-m \; \
    select-pane -t 8 \; \
    send-keys 'mongod --shardsvr --replSet ReplicaSet2 --dbpath /var/lib/replica23 --logpath /var/lib/replica23/log --port 27023 --logappend --smallfiles --oplogSize 50 --nojournal' C-m \; \
    select-pane -t 9 \; \
    send-keys 'mongod --shardsvr --replSet ReplicaSet3 --dbpath /var/lib/replica31 --logpath /var/lib/replica31/log --port 27031 --logappend --smallfiles --oplogSize 50 --nojournal' C-m \; \
    select-pane -t 10 \; \
    send-keys 'mongod --shardsvr --replSet ReplicaSet3 --dbpath /var/lib/replica32 --logpath /var/lib/replica32/log --port 27032 --logappend --smallfiles --oplogSize 50 --nojournal' C-m \; \
    select-pane -t 11 \; \
    send-keys 'mongod --shardsvr --replSet ReplicaSet3 --dbpath /var/lib/replica33 --logpath /var/lib/replica33/log --port 27033 --logappend --smallfiles --oplogSize 50 --nojournal' C-m \; \
    new-window -t $session:1 -n mongos \; \
    send-keys 'mongos --configdb ReplicaSetConfig/localhost:27018,localhost:27019,localhost:27020 --logpath /var/lib/mongos.log  --logappend --port 47017 --bind_ip localhost' C-m \; \
    #split-window -h -p 67 \; \
    #split-window -h -p 50 \; \
    #select-pane -t 0 \; \
    #send-keys 'mongos --configdb ReplicaSetConfig/localhost:27018,localhost:27012,localhost:27013 --logpath /var/lib/mongos.log  --logappend --port 47017 --bind_ip localhost' C-m \; \
    #select-pane -t 1 \; \
    #send-keys 'mongos --configdb ReplicaSet2/localhost:27021,localhost:27022,localhost:27023 --logpath /var/lib/mongos.log  --logappend --port 47017 --bind_ip localhost' C-m \; \
    #select-pane -t 2 \; \
    #send-keys 'mongos --configdb ReplicaSet3/localhost:27031,localhost:27032,localhost:27033 --logpath /var/lib/mongos.log  --logappend --port 47017 --bind_ip localhost' C-m \; \