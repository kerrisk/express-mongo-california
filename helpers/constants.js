exports.constants = {
  admin: {
    name: "admin",
    email: process.env.EMAIL_SMTP_USERNAME,
  },
  confirmEmails: {
    from: process.env.EMAIL_SMTP_USERNAME,
  },
};
